# ModbusTcp 封装好的Modbus协议类库

## 简介
本仓库提供了一个名为 `ModbusTcp.dll` 的类库，该类库封装了Modbus协议，使得C#上位机能够直接读写遵循Modbus协议的PLC设备，例如汇川等品牌的PLC。通过使用这个类库，开发者可以简化与PLC的通信过程，提高开发效率。

## 功能特点
- **简单易用**：封装了Modbus协议的复杂性，开发者只需调用简单的API即可实现与PLC的通信。
- **高效稳定**：经过优化和测试，确保通信的稳定性和高效性。
- **兼容性强**：支持多种遵循Modbus协议的PLC设备，包括但不限于汇川等品牌。

## 使用方法
1. **下载类库**：从本仓库下载 `ModbusTcp.dll` 文件。
2. **添加引用**：在C#项目中添加对 `ModbusTcp.dll` 的引用。
3. **调用API**：使用类库提供的API进行PLC数据的读写操作。

## 示例代码
以下是一个简单的示例代码，展示了如何使用 `ModbusTcp.dll` 读取PLC的数据：

```csharp
using ModbusTcp;

class Program
{
    static void Main(string[] args)
    {
        // 创建ModbusTcp对象
        ModbusTcpClient client = new ModbusTcpClient("192.168.1.100", 502);

        // 读取PLC的寄存器数据
        ushort[] data = client.ReadHoldingRegisters(1, 0, 10);

        // 输出读取到的数据
        foreach (var value in data)
        {
            Console.WriteLine(value);
        }
    }
}
```

## 注意事项
- 在使用类库前，请确保PLC设备已正确配置并连接到网络。
- 请根据实际需求调整读写操作的参数，如寄存器地址、数据长度等。

## 贡献
欢迎开发者提交问题反馈或改进建议，共同完善这个类库。

## 许可证
本项目采用MIT许可证，详情请参阅 `LICENSE` 文件。